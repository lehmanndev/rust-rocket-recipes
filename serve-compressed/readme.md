# Rocket: serve compressed

Serve a resource from a .gz file if is present in the filesystem.

This re-implements Rocket's `FileServer` serving a resource compressed, if requested by via `Accept-Encoding` and the resouce extended by `.gz` exists in the filesystem.
