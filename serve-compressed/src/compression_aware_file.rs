// Copied from Rocket [1] and reduced to minumum
// [1]: https://github.com/SergioBenitez/Rocket/tree/v0.5

use std::ffi::OsStr;
use std::io;
use std::path::{Path, PathBuf};
use std::ops::{Deref, DerefMut};

use tokio::fs::File;

use rocket::request::Request;
use rocket::response::{self, Responder};
use rocket::http::{ContentType, Header};
use rocket::http::hyper::header;

#[derive(Debug)]
pub struct CompressionAwareFile(PathBuf, File, bool);

impl CompressionAwareFile {
    /// A [`Responder`] that, if requested by `Accept-Encoding`, attempts to
    /// send a file with .gz appended, with the `Content-Type` based on the
    /// filename extension excluding the .gz and the `Content-Encoding` set 
    /// to "gzip".
    pub async fn open<P: AsRef<Path>>(path: P, req: &Request<'_>) -> io::Result<CompressionAwareFile> {
        let zipped_requested = match req.headers().get_one("Accept-Encoding") {
            Some(accepted) if accepted.contains("gzip") || accepted.contains("br") => true,
            _ => false
        };

        let filename = path.as_ref().file_name().unwrap().to_str().unwrap();
        let zipped_path = path.as_ref().parent().unwrap().join(OsStr::new(&(String::from(filename) + ".gz")));

        if zipped_requested && zipped_path.exists() {
            let zipped_path_ref = <PathBuf as AsRef<Path>>::as_ref(&zipped_path);
            let file = File::open(zipped_path_ref).await?;
            return Ok(CompressionAwareFile(path.as_ref().to_path_buf(), file, true));
        } else {
            let file = File::open(path.as_ref()).await?;
            return Ok(CompressionAwareFile(path.as_ref().to_path_buf(), file, false));
        }
    }

    #[inline(always)]
    pub fn file(&self) -> &File {
        &self.1
    }

    #[inline(always)]
    pub fn file_mut(&mut self) -> &mut File {
        &mut self.1
    }

    #[inline(always)]
    pub fn take_file(self) -> File {
        self.1
    }

    #[inline(always)]
    pub fn path(&self) -> &Path {
        self.0.as_path()
    }
}

impl<'r> Responder<'r, 'static> for CompressionAwareFile {
    fn respond_to(self, req: &'r Request<'_>) -> response::Result<'static> {
        let mut response = self.1.respond_to(req)?;
        if let Some(ext) = self.0.extension() {
            if let Some(ct) = ContentType::from_extension(&ext.to_string_lossy()) {
                response.set_header(ct);
            }
        }

        if self.2 {
            response.set_header(Header::new(header::CONTENT_ENCODING.as_str(), "gzip"));
        }

        Ok(response)
    }
}

impl Deref for CompressionAwareFile {
    type Target = File;

    fn deref(&self) -> &File {
        &self.1
    }
}

impl DerefMut for CompressionAwareFile {
    fn deref_mut(&mut self) -> &mut File {
        &mut self.1
    }
}
