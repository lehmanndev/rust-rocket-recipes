use rocket::fs::{relative};
use rocket::request::{Request};
use rust_rocket_recipe_serve_compressed::file_server::CompressionAwareFileServer;

#[macro_use] extern crate rocket;

#[catch(404)]
fn not_found(req: &Request) -> String {
    format!("The URL '{}' was not found", req.uri())
}

#[launch]
fn rocket() -> _ {
    rocket::build()
        .register("/", catchers![not_found])
        .mount("/", CompressionAwareFileServer::from(relative!("static")))
}
