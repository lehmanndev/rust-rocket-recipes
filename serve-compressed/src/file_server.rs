// Copied from Rocket [1] and reduced to minumum
// [1]: https://github.com/SergioBenitez/Rocket/tree/v0.5

use async_trait::async_trait;
use figment;
use std::path::{PathBuf, Path};
use rocket::{Data, Route};
use rocket::http::uri::Segments;
use rocket::http::Method;
use rocket::request::{Request};
use rocket::route::{self, Handler};
use super::compression_aware_file::CompressionAwareFile;

#[derive(Debug, Clone)]
pub struct CompressionAwareFileServer {
    root: PathBuf,
    rank: isize,
}

impl CompressionAwareFileServer {
    const DEFAULT_RANK: isize = 10;

    #[track_caller]
    pub fn from<P: AsRef<Path>>(path: P) -> Self {
        CompressionAwareFileServer::new(path)
    }

    #[track_caller]
    pub fn new<P: AsRef<Path>>(path: P) -> Self {
        let path = path.as_ref();
        CompressionAwareFileServer {
            root: path.into(),
            rank: Self::DEFAULT_RANK
        }
    }

    pub fn rank(mut self, rank: isize) -> Self {
        self.rank = rank;
        self
    }
}

#[async_trait]
impl Handler for CompressionAwareFileServer {
    async fn handle<'r>(&self, req: &'r Request<'_>, data: Data<'r>) -> route::Outcome<'r> {
        use rocket::http::uri::fmt::Path;

        let path = req.segments::<Segments<'_, Path>>(0..).ok()
            .and_then(|segments| segments.to_path_buf(false).ok())
            .map(|path| self.root.join(path));

        match path {
            Some(p) if p.is_dir() => {
                let index = CompressionAwareFile::open(p.join("index.html"), req).await.ok();
                route::Outcome::from_or_forward(req, data, index)
            },
            Some(p) => {
                route::Outcome::from_or_forward(req, data, CompressionAwareFile::open(p, req).await.ok())
            },
            None => route::Outcome::forward(data),
        }
    }
}

impl From<CompressionAwareFileServer> for Vec<Route> {
    fn from(server: CompressionAwareFileServer) -> Self {
        let source = figment::Source::File(server.root.clone());
        let mut route = Route::ranked(server.rank, Method::Get, "/<path..>", server);
        route.name = Some(format!("CompressionAwareFileServer: {}", source).into());
        vec![route]
    }
}

